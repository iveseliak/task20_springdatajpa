package com.example.first.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;



@Entity
public class UserInfo {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String trainingExperience;
	
	private String phoneNumber;
	
	private String address;
	
	private String sex;
	
	@ManyToMany
	@JoinTable(name = "gym_user", 
    joinColumns = @JoinColumn(name = "user_info_id"), 
    inverseJoinColumns = @JoinColumn(name = "gym_id"))
	private List<Gym> previous=new ArrayList<>();
	
	@ManyToOne
	private Gym gym;

	@ManyToOne
	private City city;

	public UserInfo() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTrainingExperience() {
		return trainingExperience;
	}

	public void setTrainingExperience(String trainingExperience) {
		this.trainingExperience = trainingExperience;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	
	

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Gym getGym() {
		return gym;
	}

	public void setGym(Gym gym) {
		this.gym = gym;
	}

	public List<Gym> getPrevious() {
		return previous;
	}

	public void setPrevious(List<Gym> previous) {
		this.previous = previous;
	}

	
	
	
}
