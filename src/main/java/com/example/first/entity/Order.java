package com.example.first.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
//	@ManyToMany
//	private Map<Product, Integer> products = new HashMap<>();

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "order_productWC", 
    joinColumns = @JoinColumn(name = "order_id"), 
    inverseJoinColumns = @JoinColumn(name = "orderWC_id"))
	private List<ProductWhithCount> productWithCount=new ArrayList<>();

	@ManyToOne
	private User user;
	
	private Long price;
	
	

	public Order() {
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}



	public List<ProductWhithCount> getProductWithCount() {
		return productWithCount;
	}



	public void setProductWithCount(List<ProductWhithCount> productWithCount) {
		this.productWithCount = productWithCount;
	}

	
	
	
	

}
