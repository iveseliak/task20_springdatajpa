package com.example.first.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.example.first.entity.enums.TypeProduct;

@Entity
public class Product {

	@Id
	@GeneratedValue
	private Long id;

	private String name;

	private String price;

	private String description;
	
	@Column(name = "type")
    @Enumerated(EnumType.STRING)
    private TypeProduct type;


	@ManyToOne
	@JoinColumn(name = "subcategory_id")
	private SubCategory subCategory;
	
	

	public Product() {
	}
	
	

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public TypeProduct getType() {
		return type;
	}



	public void setType(TypeProduct type) {
		this.type = type;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDescreption() {
		return description;
	}

	public void setDescreption(String descreption) {
		this.description = descreption;
	}

	
	

	public SubCategory getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(SubCategory subCategory) {
		this.subCategory = subCategory;
	}


	
	

}
