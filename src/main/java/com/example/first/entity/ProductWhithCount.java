package com.example.first.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ProductWhithCount {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	
	private Long count;
	
	@ManyToOne
	private Product product;
	
	public ProductWhithCount() {}
	
	

	public Product getProduct() {
		return product;
	}



	public void setProduct(Product product) {
		this.product = product;
	}



	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}




	public Long getCount() {
		return count;
	}


	public void setCount(Long count) {
		this.count = count;
	}
	
	
	
	

}
