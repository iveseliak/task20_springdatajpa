package com.example.first.entity.enums;

public enum TypeProduct {
	PRODUCT,
	SERVICE;
	
	TypeProduct() {}

}
