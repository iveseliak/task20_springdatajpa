package com.example.first.entity.enums;

public enum RoleEnum {
    TREINER,
    USER,
    ADMIN;

    RoleEnum() {
    }

}
