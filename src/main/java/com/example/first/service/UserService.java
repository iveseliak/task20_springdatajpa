package com.example.first.service;

import java.util.List;

import com.example.first.dto.UserRequestDTO;
import com.example.first.dto.UserResponseDTO;
import com.example.first.entity.User;

public interface UserService {
	
	List<UserResponseDTO> findAll();

	User findOne(Long id);

	UserResponseDTO save(UserRequestDTO userRequestDTO);

	UserResponseDTO update(Long id, UserRequestDTO userRequestDTO);

	void delete(Long id);
	
	List<UserResponseDTO> findAllByName(String name);
}
