package com.example.first.service;

import java.util.List;

import com.example.first.dto.GymRequestDTO;
import com.example.first.dto.GymResponseDTO;
import com.example.first.entity.Gym;

public interface GymService {
	
	 
	GymResponseDTO save(GymRequestDTO gymRequestDTO);

	GymResponseDTO update(Long id, GymRequestDTO gymRequestDTO);

	void delete(Long id);

	List<GymResponseDTO> findAll();

}
