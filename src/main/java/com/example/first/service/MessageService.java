package com.example.first.service;

import java.util.List;

import com.example.first.dto.ConversationResponseDTO;
import com.example.first.dto.MessageRequestDTO;
import com.example.first.dto.MessageResponseDTO;
import com.example.first.entity.Message;

public interface MessageService {
	MessageResponseDTO save(MessageRequestDTO messageRequestDTO);

	MessageResponseDTO update(Long id, MessageRequestDTO messageRequestDTO);

	void delete(Long id);

	List<MessageResponseDTO> findAll();
	
	ConversationResponseDTO findByReceiverId(Long receiverId);

	ConversationResponseDTO findAllReceiversMessages();


}
