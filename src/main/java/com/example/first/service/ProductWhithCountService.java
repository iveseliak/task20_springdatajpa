package com.example.first.service;

import java.util.List;

import com.example.first.dto.ProductWhithCountRequestDTO;
import com.example.first.dto.ProductWhithCountResponseDTO;
import com.example.first.entity.ProductWhithCount;

public interface ProductWhithCountService {
	
	public ProductWhithCountResponseDTO save(ProductWhithCountRequestDTO productWhithCountRequestDTO);
	
	ProductWhithCountResponseDTO update(Long id, ProductWhithCountRequestDTO productWhithCountRequestDTO);
	
	void delete(Long id);
	
	List<ProductWhithCountResponseDTO> findAll();
}
