package com.example.first.service;

import java.util.List;

import com.example.first.dto.ProductRequestDTO;
import com.example.first.dto.ProductResponseDTO;
import com.example.first.entity.Product;

public interface ProductService {
	ProductResponseDTO save(ProductRequestDTO productRequestDTO);

	ProductResponseDTO update(Long id, ProductRequestDTO productRequestDTO);

	void delete(Long id);

	List<ProductResponseDTO> findAll();
}
