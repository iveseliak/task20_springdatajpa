package com.example.first.service;

import java.util.List;

import com.example.first.dto.SubCategoryRequestDTO;
import com.example.first.dto.SubCategoryResponseDTO;
import com.example.first.entity.SubCategory;

public interface SubCategoryService {
	
	SubCategoryResponseDTO save(SubCategoryRequestDTO subCategoryRequestDTO);

	SubCategoryResponseDTO update(Long id, SubCategoryRequestDTO subCategoryRequestDTO);

	void delete(Long id);

	List<SubCategoryResponseDTO> findAll();

}
