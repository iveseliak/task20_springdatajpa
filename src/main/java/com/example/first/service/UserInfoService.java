package com.example.first.service;

import com.example.first.dto.UserInfoRequestDTO;
import com.example.first.dto.UserInfoResponseDTO;

public interface UserInfoService {
	
	UserInfoResponseDTO save(UserInfoRequestDTO userInfoRequestDTO);

	UserInfoResponseDTO update(Long id, UserInfoRequestDTO userInfoRequestDTO);


}
