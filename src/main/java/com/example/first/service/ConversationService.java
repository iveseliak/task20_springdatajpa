package com.example.first.service;

import java.util.List;

import com.example.first.dto.ConversationRequestDTO;
import com.example.first.dto.ConversationResponseDTO;
import com.example.first.entity.Conversation;

public interface ConversationService {
	
	ConversationResponseDTO save(ConversationRequestDTO conversationRequestDTO);

	ConversationResponseDTO update(Long id, ConversationRequestDTO conversationRequestDTO);

	void delete(Long id);

	List<ConversationResponseDTO> findAll();

}
