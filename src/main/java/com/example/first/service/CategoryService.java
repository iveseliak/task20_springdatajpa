package com.example.first.service;

import java.util.List;

import com.example.first.dto.CategoryRequestDTO;
import com.example.first.dto.CategoryResponseDTO;
import com.example.first.entity.Category;

public interface CategoryService {
	CategoryResponseDTO save(CategoryRequestDTO categoryRequestDTO);

	CategoryResponseDTO update(Long id, CategoryRequestDTO categoryRequestDTO);

	void delete(Long id);

//	List<Category> findAll();
	
	List<CategoryResponseDTO> findAll();
}
