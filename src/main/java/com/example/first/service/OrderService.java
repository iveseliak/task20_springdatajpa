package com.example.first.service;

import java.util.List;

import com.example.first.dto.OrderRequestDTO;
import com.example.first.dto.OrderResponseDTO;
import com.example.first.entity.Order;

public interface OrderService {
	
	OrderResponseDTO save(OrderRequestDTO orderRequestDTO);

	OrderResponseDTO update(Long id, OrderRequestDTO orderRequestDTO);

	void delete(Long id);

	List<OrderResponseDTO> findAll();

}
