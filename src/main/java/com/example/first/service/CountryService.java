package com.example.first.service;

import com.example.first.dto.CountryRequestDTO;
import com.example.first.dto.CountryResponseDTO;

public interface CountryService {
	CountryResponseDTO save(CountryRequestDTO countryRequestDTO);

	CountryResponseDTO update(Long id, CountryRequestDTO countryRequestDTO);

	void delete(Long id);

	
}
