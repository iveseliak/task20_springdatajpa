package com.example.first.service;

import java.util.List;

import com.example.first.dto.CityRequestDTO;
import com.example.first.dto.CityResponseDTO;
import com.example.first.entity.City;

public interface CityService {
	CityResponseDTO save(CityRequestDTO cityRequestDTO);

	CityResponseDTO update(Long id, CityRequestDTO cityRequestDTO);

	void delete(Long id);

	List<CityResponseDTO> findAll();
}
