package com.example.first.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.first.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

}
