package com.example.first.repository;

import org.jboss.logging.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.first.entity.Conversation;
import com.example.first.entity.User;

@Repository
public interface ConversationRepository extends JpaRepository<Conversation, Long>{
	
//	@Query("Select c from Conversation c Where (c.author = user1 And c.receiver = user2) Or (c.author = user2 And c.receiver = user1)");
//	Conversation findConversationByAuthorAndReceiver(@Param("user1") User user1, @Param("user2") User user2);


}
