package com.example.first.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.first.entity.ProductWhithCount;

public interface ProductWhithCountRepository extends JpaRepository<ProductWhithCount, Long>{

}
