package com.example.first.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.first.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long>{

}
