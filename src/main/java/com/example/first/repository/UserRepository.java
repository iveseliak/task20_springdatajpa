package com.example.first.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.first.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
//	@Query("Select u From User u Where u.login = ?#{principal.username}")
//	User findCurrentUser();
	
//	@Query("select u from User u where u.name=:nameUser")
//	List<User>findUserByName(@Param("nameUser") String nameUser);
	
	
	List<User>findAllByName(String name);
	
	

}
