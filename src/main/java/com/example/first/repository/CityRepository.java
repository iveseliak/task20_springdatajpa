package com.example.first.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.first.entity.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long>{

}
