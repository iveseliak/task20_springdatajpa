package com.example.first.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.first.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{

}
