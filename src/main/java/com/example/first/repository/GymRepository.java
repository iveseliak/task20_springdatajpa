package com.example.first.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.first.entity.Gym;

@Repository
public interface GymRepository extends JpaRepository<Gym, Long>{

}
