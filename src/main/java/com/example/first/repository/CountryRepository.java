package com.example.first.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.first.entity.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long>{

}
