package com.example.first.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.CountryRequestDTO;
import com.example.first.dto.CountryResponseDTO;
import com.example.first.entity.Country;
import com.example.first.repository.CountryRepository;
import com.example.first.service.CountryService;
@Service
public class CountryServiceImpl implements CountryService{
	
	@Autowired
	CountryRepository countryRepository;

	@Override
	public CountryResponseDTO save(CountryRequestDTO countryRequestDTO) {
		Country country=new Country();
		country.setName(countryRequestDTO.getName());
		return new CountryResponseDTO(countryRepository.save(country));
	}

	@Override
	public CountryResponseDTO update(Long id, CountryRequestDTO countryRequestDTO) {
		Country country=countryRepository.findById(id).get();
		country.setName(countryRequestDTO.getName());
		return new CountryResponseDTO(countryRepository.save(country));
	}

	@Override
	public void delete(Long id) {
		countryRepository.deleteById(id);
		
	}

}
