package com.example.first.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.CategoryResponseDTO;
import com.example.first.dto.CityRequestDTO;
import com.example.first.dto.CityResponseDTO;
import com.example.first.entity.Category;
import com.example.first.entity.City;
import com.example.first.repository.CityRepository;
import com.example.first.repository.CountryRepository;
import com.example.first.repository.GymRepository;
import com.example.first.service.CityService;
@Service
public class CityServiceImpl implements CityService{
	
	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private GymRepository gymRepository;

	@Override
	public CityResponseDTO save(CityRequestDTO cityRequestDTO) {
		City city=new City();
		city.setName(cityRequestDTO.getName());
		city.setCountry(countryRepository.findById(cityRequestDTO.getIdCountry()).get());
		city.setGymList(gymRepository.findAll());
		return new CityResponseDTO(cityRepository.save(city));
	}

	@Override
	public CityResponseDTO update(Long id, CityRequestDTO cityRequestDTO) {
		City city=cityRepository.findById(id).get();
		city.setName(cityRequestDTO.getName());
		city.setCountry(countryRepository.findById(id).get());
		return new CityResponseDTO(cityRepository.save(city));
	}

	@Override
	public void delete(Long id) {
		cityRepository.deleteById(id);
		
	}


	
	@Override
    public List<CityResponseDTO> findAll(){
        List<City> cities = cityRepository.findAll();
        List<CityResponseDTO> cityResponses = new ArrayList<>();
        for (City city:cities){
        	cityResponses.add(new CityResponseDTO(city));
        }
        return cityResponses;
    }

}
