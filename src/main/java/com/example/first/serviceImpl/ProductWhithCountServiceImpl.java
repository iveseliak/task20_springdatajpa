package com.example.first.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.ProductResponseDTO;
import com.example.first.dto.ProductWhithCountRequestDTO;
import com.example.first.dto.ProductWhithCountResponseDTO;
import com.example.first.entity.Product;
import com.example.first.entity.ProductWhithCount;
import com.example.first.repository.ProductRepository;
import com.example.first.repository.ProductWhithCountRepository;
import com.example.first.service.ProductWhithCountService;
@Service
public class ProductWhithCountServiceImpl implements ProductWhithCountService{
	
	@Autowired
	ProductWhithCountRepository productWhithCountRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	@Override
	public ProductWhithCountResponseDTO save(ProductWhithCountRequestDTO productWhithCountRequestDTO) {
		ProductWhithCount productWhithCount=new ProductWhithCount();
		productWhithCount.setCount(productWhithCountRequestDTO.getCount());
		productWhithCount.setProduct(productRepository.findById(productWhithCountRequestDTO.getProductId()).get());
		
		return new ProductWhithCountResponseDTO(productWhithCountRepository.save(productWhithCount));
	}

	@Override
	public ProductWhithCountResponseDTO update(Long id, ProductWhithCountRequestDTO productWhithCountRequestDTO) {
		ProductWhithCount productWhithCount=productWhithCountRepository.findById(id).get();
		productWhithCount.setCount(productWhithCountRequestDTO.getCount());
		productWhithCount.setProduct(productRepository.findById(productWhithCountRequestDTO.getProductId()).get());
		
		return new ProductWhithCountResponseDTO(productWhithCountRepository.save(productWhithCount));
	}

	@Override
	public void delete(Long id) {
		productWhithCountRepository.deleteById(id);
		
	}

//	@Override
//	public List<ProductWhithCount> findAll() {
//		
//		return productWhithCountRepository.findAll();
//	}
	
	@Override
    public List<ProductWhithCountResponseDTO> findAll(){
        List<ProductWhithCount> productWhithCounts = productWhithCountRepository.findAll();
        List<ProductWhithCountResponseDTO> productWhithCountResponses = new ArrayList<>();
        for (ProductWhithCount productWhithCount:productWhithCounts){
        	productWhithCountResponses.add(new ProductWhithCountResponseDTO(productWhithCount));
        }
        return productWhithCountResponses;
    }
	
	

}
