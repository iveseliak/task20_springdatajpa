package com.example.first.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.MessageResponseDTO;
import com.example.first.dto.OrderRequestDTO;
import com.example.first.dto.OrderResponseDTO;
import com.example.first.entity.Message;
import com.example.first.entity.Order;
import com.example.first.repository.OrderRepository;
import com.example.first.repository.UserRepository;
import com.example.first.service.OrderService;
@Service
public class OrderServiceImpl implements OrderService{
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	UserRepository userRepository;

	@Override
	public OrderResponseDTO save(OrderRequestDTO orderRequestDTO) {
		Order order=new Order();
//		order.setProducts(orderRequestDTO.getProducts());
		order.setUser(userRepository.findById(orderRequestDTO.getUserId()).get());
		order.setPrice(orderRequestDTO.getPrice());
		return new OrderResponseDTO(orderRepository.save(order));
	}

	@Override
	public OrderResponseDTO update(Long id, OrderRequestDTO orderRequestDTO) {
		Order order= orderRepository.findById(id).get();
//		order.setProducts(orderRequestDTO.getProducts());
		order.setUser(userRepository.findById(orderRequestDTO.getUserId()).get());
		order.setPrice(orderRequestDTO.getPrice());
		return new OrderResponseDTO(orderRepository.save(order));
	}

	@Override
	public void delete(Long id) {
		orderRepository.deleteById(id);
		
	}

//	@Override
//	public List<Order> findAll() {
//		
//		return orderRepository.findAll();
//	}
	
	@Override
    public List<OrderResponseDTO> findAll(){
        List<Order> orders = orderRepository.findAll();
        List<OrderResponseDTO> orderResponses = new ArrayList<>();
        for (Order order:orders){
        	orderResponses.add(new OrderResponseDTO(order));
        }
        return orderResponses;
    }

}
