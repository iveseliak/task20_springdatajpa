package com.example.first.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.CategoryRequestDTO;
import com.example.first.dto.CategoryResponseDTO;
import com.example.first.entity.Category;
import com.example.first.repository.CategoryRepository;
import com.example.first.service.CategoryService;
@Service
public class CategoryServiceImpl implements CategoryService{
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public CategoryResponseDTO save(CategoryRequestDTO categoryRequestDTO) {
		Category category=new Category();
		category.setName(categoryRequestDTO.getName());
		category=categoryRepository.save(category);
		return new CategoryResponseDTO(categoryRepository.save(category));
	}

	@Override
	public CategoryResponseDTO update(Long id, CategoryRequestDTO categoryRequestDTO) {
		Category category=categoryRepository.findById(id).get();
		category.setName(categoryRequestDTO.getName());
		
		return new CategoryResponseDTO(categoryRepository.save(category));
	}

	@Override
	public void delete(Long id) {
		categoryRepository.deleteById(id);
		
		
	}

//	@Override
//	public List<Category> findAll() {
//		
//		return categoryRepository.findAll();
//	}
	
	@Override
    public List<CategoryResponseDTO> findAll(){
        List<Category> categories = categoryRepository.findAll();
        List<CategoryResponseDTO> categoryResponses = new ArrayList<>();
        for (Category category:categories){
        	categoryResponses.add(new CategoryResponseDTO(category));
        }
        return categoryResponses;
    }

}
