package com.example.first.serviceImpl;



import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.ConversationResponseDTO;
import com.example.first.dto.GymResponseDTO;
import com.example.first.dto.MessageRequestDTO;
import com.example.first.dto.MessageResponseDTO;
import com.example.first.entity.Conversation;
import com.example.first.entity.Gym;
import com.example.first.entity.Message;
import com.example.first.entity.User;
import com.example.first.repository.ConversationRepository;
import com.example.first.repository.MessageRepository;
import com.example.first.repository.UserRepository;
import com.example.first.service.MessageService;
@Service
	public class MessageServiceImpl implements MessageService {

		@Autowired
		private MessageRepository messageRepository;

		@Autowired
		private UserRepository userRepository;

		@Autowired
		private ConversationRepository conversationRepository;

//		@Override
//		public MessageResponseDTO save(MessageRequestDTO messageRequest) {
//			Message message = new Message();
//			message.setIsReviewed(false);
//			message.setText(messageRequest.getMessage());
//			User currentUser = userRepository.findCurrentUser();
//			message.setAuthor(currentUser);
//			User receiver = userRepository.findById(messageRequest.getReceiverId()).get();
//			Conversation conversation = conversationRepository.findConversationByAuthorAndReceiver(currentUser, receiver);
//			if (conversation == null) {
//				conversation = new Conversation();
//				conversation.setSender(currentUser);
//				conversation.setReceiver(receiver);
//				conversation.setCreatedAt(LocalDateTime.now());
//				conversationRepository.save(conversation);
//			}
//			message.setConversation(conversation);
//			message.setCreatedAt(LocalDateTime.now());
//			messageRepository.save(message);
//			return MessageResponseDTO.fromMessage(message);
//		}


		@Override
		public void delete(Long id) {
			messageRepository.deleteById(id);

		}

		@Override
		public ConversationResponseDTO findByReceiverId(Long receiverId) {
			
			return null;
		}

		@Override
		public ConversationResponseDTO findAllReceiversMessages() {
			return null;
		}

//		@Override
//		public MessageResponseDTO update(Long id, MessageRequestDTO messageRequestDTO) {
//			// TODO Auto-generated method stub
//			return null;
//		}

//		@Override
//		public List<Message> findAll() {
//			
//			return messageRepository.findAll();
//		}
		
		@Override
	    public List<MessageResponseDTO> findAll(){
	        List<Message> messages = messageRepository.findAll();
	        List<MessageResponseDTO> messageResponses = new ArrayList<>();
	        for (Message message:messages){
	        	messageResponses.add(new MessageResponseDTO(message));
	        }
	        return messageResponses;
	    }

		@Override
		public MessageResponseDTO save(MessageRequestDTO messageRequestDTO) {
			Message message=new Message();
			message.setText(messageRequestDTO.getMessage());
			message.setAuthor(userRepository.findById(messageRequestDTO.getAuthorId()).get());
			return  new MessageResponseDTO(messageRepository.save(message));
		}

		@Override
		public MessageResponseDTO update(Long id, MessageRequestDTO messageRequestDTO) {
			Message message=messageRepository.findById(id).get();
			message.setText(messageRequestDTO.getMessage());
			message.setAuthor(userRepository.findById(messageRequestDTO.getAuthorId()).get());
			return  new MessageResponseDTO(messageRepository.save(message));
		}

	}


