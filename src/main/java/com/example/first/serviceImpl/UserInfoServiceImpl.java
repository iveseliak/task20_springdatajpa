package com.example.first.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.UserInfoRequestDTO;
import com.example.first.dto.UserInfoResponseDTO;
import com.example.first.entity.UserInfo;
import com.example.first.repository.CityRepository;
import com.example.first.repository.GymRepository;
import com.example.first.repository.UserInfoRepository;
import com.example.first.service.UserInfoService;
@Service
public class UserInfoServiceImpl implements UserInfoService{
	
	@Autowired
	UserInfoRepository userInfoRepository;
	
	@Autowired
	CityRepository cityRepository;
	
	@Autowired
	GymRepository gymRepository;

	@Override
	public UserInfoResponseDTO save(UserInfoRequestDTO userInfoRequestDTO) {
		UserInfo userInfo=new UserInfo();
		userInfo.setCity(cityRepository.findById(userInfoRequestDTO.getCityId()).get());
		userInfo.setGym(gymRepository.findById(userInfoRequestDTO.getGymId()).get());
		userInfo.setAddress(userInfoRequestDTO.getAddress());
		userInfo.setPhoneNumber(userInfoRequestDTO.getPhoneNumber());
		userInfo.setSex(userInfoRequestDTO.getSex());
		userInfo.setTrainingExperience(userInfoRequestDTO.getTrainingExperience());
		userInfo.setPrevious(userInfoRequestDTO.getPrevious());
		return new UserInfoResponseDTO(userInfoRepository.save(userInfo));
	}

	@Override
	public UserInfoResponseDTO update(Long id, UserInfoRequestDTO userInfoRequestDTO) {
		UserInfo userInfo=userInfoRepository.findById(id).get();
		userInfo.setCity(cityRepository.findById(userInfoRequestDTO.getCityId()).get());
		userInfo.setGym(gymRepository.findById(userInfoRequestDTO.getGymId()).get());
		userInfo.setAddress(userInfoRequestDTO.getAddress());
		userInfo.setPhoneNumber(userInfoRequestDTO.getPhoneNumber());
		userInfo.setSex(userInfoRequestDTO.getSex());
		userInfo.setTrainingExperience(userInfoRequestDTO.getTrainingExperience());
		userInfo.setPrevious(userInfoRequestDTO.getPrevious());
		return new UserInfoResponseDTO(userInfoRepository.save(userInfo));
	}

}
