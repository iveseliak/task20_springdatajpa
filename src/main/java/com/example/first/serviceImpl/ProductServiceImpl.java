package com.example.first.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.OrderResponseDTO;
import com.example.first.dto.ProductRequestDTO;
import com.example.first.dto.ProductResponseDTO;
import com.example.first.entity.Order;
import com.example.first.entity.Product;
import com.example.first.repository.ProductRepository;
import com.example.first.repository.SubCategoryRepository;
import com.example.first.service.ProductService;
@Service
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	SubCategoryRepository subCategoryRepository;

	@Override
	public ProductResponseDTO save(ProductRequestDTO productRequestDTO) {
		Product product=new Product();
		product.setName(productRequestDTO.getName());
		product.setPrice(productRequestDTO.getPrice());
		product.setSubCategory(subCategoryRepository.findById(productRequestDTO.getIdSubCategory()).get());
		product.setDescreption(productRequestDTO.getDescreption());
		product.setType(productRequestDTO.getType());
		return new ProductResponseDTO(productRepository.save(product));
	}

	@Override
	public ProductResponseDTO update(Long id, ProductRequestDTO productRequestDTO) {
		Product product=productRepository.findById(id).get();
		product.setName(productRequestDTO.getName());
		product.setPrice(productRequestDTO.getPrice());
		product.setSubCategory(subCategoryRepository.findById(productRequestDTO.getIdSubCategory()).get());
		product.setDescreption(productRequestDTO.getDescreption());
		product.setType(productRequestDTO.getType());
		return new ProductResponseDTO(productRepository.save(product));
	}

	@Override
	public void delete(Long id) {
		productRepository.deleteById(id);
		
	}

//	@Override
//	public List<Product> findAll() {
//		
//		return productRepository.findAll();
//	}
	
	@Override
    public List<ProductResponseDTO> findAll(){
        List<Product> products = productRepository.findAll();
        List<ProductResponseDTO> productResponses = new ArrayList<>();
        for (Product product:products){
        	productResponses.add(new ProductResponseDTO(product));
        }
        return productResponses;
    }

}
