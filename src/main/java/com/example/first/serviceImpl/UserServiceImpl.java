package com.example.first.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.ProductResponseDTO;
import com.example.first.dto.UserRequestDTO;
import com.example.first.dto.UserResponseDTO;
import com.example.first.entity.Product;
import com.example.first.entity.User;
import com.example.first.repository.UserInfoRepository;
import com.example.first.repository.UserRepository;
import com.example.first.service.UserService;
@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepository;
	
//	@Override
//	public List<User> findAll() {
//		
//		return userRepository.findAll();
//	}
	
	@Override
    public List<UserResponseDTO> findAll(){
        List<User> users = userRepository.findAll();
        List<UserResponseDTO> userResponses = new ArrayList<>();
        for (User user:users){
        	userResponses.add(new UserResponseDTO(user));
        }
        return userResponses;
    }

	@Override
	public User findOne(Long id) {
		return userRepository.findById(id).get();
	}

	@Override
	public UserResponseDTO save(UserRequestDTO userRequestDTO) {
		User user=new User();
		user.setFirstName(userRequestDTO.getFirstName());
		user.setLastName(userRequestDTO.getLastName());
		user.setLogin(userRequestDTO.getLogin());
		user.setPassword(userRequestDTO.getPassword());
		return new UserResponseDTO(userRepository.save(user));
	}

	@Override
	public UserResponseDTO update(Long id, UserRequestDTO userRequestDTO) {
		User user=userRepository.findById(id).get();
		user.setFirstName(userRequestDTO.getFirstName());
		user.setLastName(userRequestDTO.getLastName());
		user.setLogin(userRequestDTO.getLogin());
		user.setPassword(userRequestDTO.getPassword());
		return new UserResponseDTO(userRepository.save(user));
	}

	@Override
	public void delete(Long id) {
		userRepository.deleteById(id);
		
	}

	@Override
	public List<UserResponseDTO> findAllByName(String name) {
		
		return userRepository.findAllByName(name).stream().map(UserResponseDTO::new).collect(Collectors.toList());
	}
	
	

	
	
	

}
