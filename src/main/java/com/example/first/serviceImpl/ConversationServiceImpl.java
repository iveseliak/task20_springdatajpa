package com.example.first.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.ConversationRequestDTO;
import com.example.first.dto.ConversationResponseDTO;
import com.example.first.entity.Conversation;
import com.example.first.repository.ConversationRepository;
import com.example.first.repository.UserRepository;
import com.example.first.service.ConversationService;

@Service
public class ConversationServiceImpl implements ConversationService{
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ConversationRepository conversationRepository;
	
	@Override
	public ConversationResponseDTO save(ConversationRequestDTO conversationRequestDTO) {
		Conversation conversation=new Conversation();
		conversation.setAuthor(userRepository.findById(conversationRequestDTO.getAuthorId()).get());
		conversation.setReceiver(userRepository.findById(conversationRequestDTO.getReceiverId()).get());
		conversation.setMessage(conversationRequestDTO.getMessages());
		
		return new ConversationResponseDTO(conversationRepository.save(conversation));
	}

	@Override
	public ConversationResponseDTO update(Long id, ConversationRequestDTO conversationRequestDTO) {
		Conversation conversation=conversationRepository.findById(id).get();
		conversation.setAuthor(userRepository.findById(conversationRequestDTO.getAuthorId()).get());
		conversation.setReceiver(userRepository.findById(conversationRequestDTO.getReceiverId()).get());
		conversation.setMessage(conversationRequestDTO.getMessages());
		
		return new ConversationResponseDTO(conversationRepository.save(conversation));
	}

	@Override
	public void delete(Long id) {
		conversationRepository.deleteById(id);
		
	}

//	@Override
//	public List<Conversation> findAll() {
//		
//		return null;
//	}
	
	@Override
    public List<ConversationResponseDTO> findAll(){
        List<Conversation> conversations = conversationRepository.findAll();
        List<ConversationResponseDTO> conversationResponses = new ArrayList<>();
        for (Conversation conversation:conversations){
        	conversationResponses.add(new ConversationResponseDTO(conversation));
        }
        return conversationResponses;
    }

}
