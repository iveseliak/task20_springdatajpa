package com.example.first.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.SubCategoryRequestDTO;
import com.example.first.dto.SubCategoryResponseDTO;
import com.example.first.entity.SubCategory;
import com.example.first.repository.CategoryRepository;
import com.example.first.repository.SubCategoryRepository;
import com.example.first.service.SubCategoryService;
@Service
public class SubCategoryServiceImpl implements SubCategoryService{
	
	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private SubCategoryRepository subCategoryRepository;

	@Override
	public SubCategoryResponseDTO save(SubCategoryRequestDTO subCategoryRequestDTO) {
		SubCategory subCategory=new SubCategory();
		subCategory.setName(subCategoryRequestDTO.getName());
		subCategory.setCategory(categoryRepository.findById(subCategoryRequestDTO.getIdCategory()).get());
		return new SubCategoryResponseDTO(subCategoryRepository.save(subCategory));
	}

	@Override
	public SubCategoryResponseDTO update(Long id, SubCategoryRequestDTO subCategoryRequestDTO) {
		SubCategory subCategry = subCategoryRepository.findById(id).get();
		subCategry.setName(subCategoryRequestDTO.getName());
		subCategry.setCategory(categoryRepository.findById(id).get());
		return new SubCategoryResponseDTO(subCategoryRepository.save(subCategry));
	}


	@Override
	public void delete(Long id) {
		subCategoryRepository.deleteById(id);
	}


//	@Override
//	public List<SubCategory> findAll() {
//
//		return subCategoryRepository.findAll();
//	}
	
	@Override
    public List<SubCategoryResponseDTO> findAll(){
        List<SubCategory> subCategories = subCategoryRepository.findAll();
        List<SubCategoryResponseDTO> subCategoryResponses = new ArrayList<>();
        for (SubCategory subCategory:subCategories){
        	subCategoryResponses.add(new SubCategoryResponseDTO(subCategory));
        }
        return subCategoryResponses;
    }


	

}
