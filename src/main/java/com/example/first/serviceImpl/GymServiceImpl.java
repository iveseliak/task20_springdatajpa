package com.example.first.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.first.dto.CityResponseDTO;
import com.example.first.dto.GymRequestDTO;
import com.example.first.dto.GymResponseDTO;
import com.example.first.entity.City;
import com.example.first.entity.Gym;
import com.example.first.repository.GymRepository;
import com.example.first.service.GymService;
@Service
public class GymServiceImpl implements GymService{
	
	@Autowired
	GymRepository gymRepository;

	@Override
	public GymResponseDTO save(GymRequestDTO gymRequestDTO) {
		Gym gym=new Gym();
		gym.setAdress(gymRequestDTO.getAdress());
		gym.setCityList(gymRequestDTO.getCityList());
		gym.setName(gymRequestDTO.getName());
		gym.setProductList(gymRequestDTO.getProductList());
		return new GymResponseDTO(gymRepository.save(gym));
	}

	@Override
	public GymResponseDTO update(Long id, GymRequestDTO gymRequestDTO) {
		Gym gym=gymRepository.findById(id).get();
		gym.setAdress(gymRequestDTO.getAdress());
		gym.setCityList(gymRequestDTO.getCityList());
		gym.setName(gymRequestDTO.getName());
		gym.setProductList(gymRequestDTO.getProductList());
		return new GymResponseDTO(gymRepository.save(gym));
	}

	@Override
	public void delete(Long id) {
		gymRepository.deleteById(id);
		
	}

//	@Override
//	public List<Gym> findAll() {
//		
//		return gymRepository.findAll();
//	}
	
	@Override
    public List<GymResponseDTO> findAll(){
        List<Gym> gyms = gymRepository.findAll();
        List<GymResponseDTO> gymResponses = new ArrayList<>();
        for (Gym gym:gyms){
        	gymResponses.add(new GymResponseDTO(gym));
        }
        return gymResponses;
    }

}
