package com.example.first.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.example.first.entity.Conversation;

public class ConversationResponseDTO {
	
	
	
	private Long Id;
	private Long authorId;
	private Long receiverId;
	private List<MessageResponseDTO> messages = new ArrayList<>();

	public ConversationResponseDTO(Conversation conversation) {
		this.Id=conversation.getId();
		this.authorId=conversation.getAuthor().getId();
		this.receiverId=conversation.getReceiver().getId();
		for (Iterator iterator = messages.iterator(); iterator.hasNext();) {
			MessageResponseDTO messageResponseDTO = (MessageResponseDTO) iterator.next();
			messages.add(messageResponseDTO);
		}
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Long getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(Long receiverId) {
		this.receiverId = receiverId;
	}

	public List<MessageResponseDTO> getMessages() {
		return messages;
	}

	public void setMessages(List<MessageResponseDTO> messages) {
		this.messages = messages;
	}
	
	
}
