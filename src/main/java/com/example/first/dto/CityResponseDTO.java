package com.example.first.dto;

import com.example.first.entity.City;
import com.example.first.entity.Country;

public class CityResponseDTO {
	
	private Long id;

	private String name;

	private Long idCountry;

	public CityResponseDTO(City city) {
		this.id = city.getId();
		this.name = city.getName();
		this.idCountry = city.getCountry().getId();
	}

	public CityResponseDTO(Country country) {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(Long idCountry) {
		this.idCountry = idCountry;
	}

	


}
