package com.example.first.dto;

import com.example.first.entity.enums.TypeProduct;

public class ProductRequestDTO {
	

		private String name;

		private String price;

		private String descreption;
		
		private TypeProduct type;

		private Long idSubCategory;

		

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public TypeProduct getType() {
			return type;
		}

		public void setType(TypeProduct type) {
			this.type = type;
		}

		public String getPrice() {
			return price;
		}

		public void setPrice(String price) {
			this.price = price;
		}

		public String getDescreption() {
			return descreption;
		}

		public void setDescreption(String descreption) {
			this.descreption = descreption;
		}

		public Long getIdSubCategory() {
			return idSubCategory;
		}

		public void setIdSubCategory(Long idSubCategory) {
			this.idSubCategory = idSubCategory;
		}

	}


