package com.example.first.dto;

public class SubCategoryRequestDTO {

	private String name;

	
	private Long categoryId;


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Long getIdCategory() {
		return categoryId;
	}


	public void setIdCategory(Long idCategory) {
		this.categoryId = idCategory;
	}
	
	
}
