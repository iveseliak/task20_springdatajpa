package com.example.first.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.example.first.entity.Message;
import com.example.first.entity.User;

public class ConversationRequestDTO {
	
	private Long authorId;

	
	private Long receiverId;

	
	private LocalDateTime createdAt;
	
	private List<Message> messages = new ArrayList<>();


	


	public Long getAuthorId() {
		return authorId;
	}


	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}


	public Long getReceiverId() {
		return receiverId;
	}


	public void setReceiverId(Long receiverId) {
		this.receiverId = receiverId;
	}


	public LocalDateTime getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}


	public List<Message> getMessages() {
		return messages;
	}


	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}


	
	
	

}
