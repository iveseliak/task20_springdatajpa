package com.example.first.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.first.entity.Order;
import com.example.first.entity.Product;
import com.example.first.entity.ProductWhithCount;

public class OrderResponseDTO {

	private long id;
	

	private Long price;
	
//	private UserResponseDTO userResponseDTO;
	
	private Long userId;
	
	private List<ProductWhithCount> productWithCount=new ArrayList<>();
	
	public OrderResponseDTO() {
	}
	
	public OrderResponseDTO(Order order) {
		this.id =(order.getId());
		this.price = (order.getPrice());
		this.userId=order.getUser().getId();
		this.productWithCount=order.getProductWithCount();
		
//		for(Map.Entry<Product, Integer> entry: order.getProducts().entrySet()) {
//			products.put(new ProductResponseDTO((Product)entry.getKey()), (Integer)entry.getValue());
//		}
	}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	


	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<ProductWhithCount> getProductWithCount() {
		return productWithCount;
	}

	public void setProductWithCount(List<ProductWhithCount> productWithCount) {
		this.productWithCount = productWithCount;
	}

	public Long getPrice() {
		return price;
	}


	public void setPrice(Long price) {
		this.price = price;
	}


	

}
