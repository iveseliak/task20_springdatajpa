package com.example.first.dto;

import java.util.Iterator;
import java.util.List;

import com.example.first.entity.City;
import com.example.first.entity.Gym;
import com.example.first.entity.UserInfo;

public class UserInfoResponseDTO {
	
	private Long id;
	
	private String trainingExperience;
	
	private String phoneNumber;
	
	private String address;
	
	private String sex;
	
	private List<Gym> previous;

	private Long cityId;
	
	private Long gymId;
	
	public UserInfoResponseDTO(UserInfo userInfo) {
		
		this.id=userInfo.getId();
		
		this.trainingExperience=userInfo.getTrainingExperience();
		
		this.phoneNumber=userInfo.getPhoneNumber();
		
		this.address=userInfo.getAddress();
		
		this.sex=userInfo.getSex();
		
		this.cityId=userInfo.getCity().getId();
		
		this.gymId=userInfo.getGym().getId();
		
		}
		
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTrainingExperience() {
		return trainingExperience;
	}

	public void setTrainingExperience(String trainingExperience) {
		this.trainingExperience = trainingExperience;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public List<Gym> getPrevious() {
		return previous;
	}

	public void setPrevious(List<Gym> previous) {
		this.previous = previous;
	}



	public Long getCityId() {
		return cityId;
	}



	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}



	public Long getGymId() {
		return gymId;
	}



	public void setGymId(Long gymId) {
		this.gymId = gymId;
	}

	

	
	

}
