package com.example.first.dto;

import com.example.first.entity.Message;

public class MessageResponseDTO {
	private Long id;
	private String text;
	private Boolean isReviewed;
	private Long authorId;
	
	

	public MessageResponseDTO(Message message) {
		this.id = id;
		this.text = text;
		this.isReviewed = isReviewed;
		this.authorId = authorId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean getIsReviewed() {
		return isReviewed;
	}

	public void setIsReviewed(Boolean isReviewed) {
		this.isReviewed = isReviewed;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

//	public  MessageResponseDTO fromMessage(Message message) {
//		MessageResponseDTO dto = new MessageResponseDTO();
//		dto.setId(message.getId());
//		dto.setIsReviewed(message.getIsReviewed());
//		dto.setText(message.getText());
//		dto.setAuthorId(message.getAuthor().getId());
//		return dto;
//	}

}


