package com.example.first.dto;

import java.util.List;

import com.example.first.entity.City;
import com.example.first.entity.Gym;

public class UserInfoRequestDTO {
	
	private String age;

	private String trainingExperience;

	private String phoneNumber;

	private String address;

	private String sex;

	private Long personId;
	
	private Long cityId;
	
	private Long gymId;
	
	List<Gym> previous;

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getTrainingExperience() {
		return trainingExperience;
	}

	public void setTrainingExperience(String trainingExperience) {
		this.trainingExperience = trainingExperience;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getGymId() {
		return gymId;
	}

	public void setGymId(Long gymId) {
		this.gymId = gymId;
	}

	public List<Gym> getPrevious() {
		return previous;
	}

	public void setPrevious(List<Gym> previous) {
		this.previous = previous;
	}
	
	
	

}
