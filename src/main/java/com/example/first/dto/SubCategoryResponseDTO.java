package com.example.first.dto;

import com.example.first.entity.SubCategory;

public class SubCategoryResponseDTO {
	
	private Long id;

    private String name;
    
    private Long categoryId;

    

	public SubCategoryResponseDTO(SubCategory subCategory) {
		this.id = subCategory.getId();
		this.name = subCategory.getName();;
		this.categoryId = subCategory.getCategory().getId();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	
	
    
}
