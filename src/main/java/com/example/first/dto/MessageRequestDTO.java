package com.example.first.dto;

public class MessageRequestDTO {
	
	private String text;
	private Boolean isReviewed;
	private Long authorId;


	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean getIsReviewed() {
		return isReviewed;
	}

	public void setIsReviewed(Boolean isReviewed) {
		this.isReviewed = isReviewed;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getMessage() {
		return text;
	}

	public void setMessage(String message) {
		this.text = message;
	}

	

}

