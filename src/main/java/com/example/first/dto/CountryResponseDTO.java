package com.example.first.dto;

import com.example.first.entity.Country;

public class CountryResponseDTO {
	
	private Long id;

	private String name;

	public CountryResponseDTO(Country country) {
		this.id = country.getId();
		this.name = country.getName();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
