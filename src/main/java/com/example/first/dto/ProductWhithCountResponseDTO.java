package com.example.first.dto;

import com.example.first.entity.ProductWhithCount;

public class ProductWhithCountResponseDTO {
	
	private Long id;
	
	private Long productId;
	
	private Long count;
	
	public ProductWhithCountResponseDTO(ProductWhithCount productWhithCount) {
		this.id=productWhithCount.getId();
		this.productId=productWhithCount.getProduct().getId();
		this.count=productWhithCount.getCount();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	
}
