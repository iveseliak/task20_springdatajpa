package com.example.first.dto;

import javax.persistence.ManyToOne;

import com.example.first.entity.Product;

public class ProductWhithCountRequestDTO {
	
	private Long count;
	
	
	private Long productId;


	public Long getCount() {
		return count;
	}


	public void setCount(Long count) {
		this.count = count;
	}


	public Long getProductId() {
		return productId;
	}


	public void setProductId(Long productId) {
		this.productId = productId;
	}
	
	

}
