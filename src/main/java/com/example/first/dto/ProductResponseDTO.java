package com.example.first.dto;

import com.example.first.entity.Product;
import com.example.first.entity.enums.TypeProduct;

public class ProductResponseDTO {
	
	private Long id;
	
	private String name;

	private String price;

	private String descreption;
	
	private TypeProduct type;

	private Long idSubCategory;
	
	
	public ProductResponseDTO(Product product) {
		
		this.id=product.getId();
		this.name=product.getName();
		this.price=product.getPrice();
		this.descreption=product.getDescreption();
		this.type=product.getType();
		this.idSubCategory=product.getSubCategory().getId();
		
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPrice() {
		return price;
	}


	public void setPrice(String price) {
		this.price = price;
	}


	public String getDescreption() {
		return descreption;
	}


	public void setDescreption(String descreption) {
		this.descreption = descreption;
	}


	public TypeProduct getType() {
		return type;
	}


	public void setType(TypeProduct type) {
		this.type = type;
	}


	public Long getIdSubCategory() {
		return idSubCategory;
	}


	public void setIdSubCategory(Long idSubCategory) {
		this.idSubCategory = idSubCategory;
	}
	
	

}
