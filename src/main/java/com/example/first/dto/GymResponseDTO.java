package com.example.first.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.example.first.entity.City;
import com.example.first.entity.Gym;
import com.example.first.entity.Product;

public class GymResponseDTO {


	private String name;
	
	private String adress;
	
	private List<ProductResponseDTO> productList=new ArrayList<>();
	
	private List<CityResponseDTO> cityList=new ArrayList<>();
	
	
	public GymResponseDTO(Gym gym) {
		this.name=gym.getName();
		this.adress=gym.getAdress();
		for (Iterator iterator = gym.getProductList().iterator(); iterator.hasNext();) {
			Product product = (Product) iterator.next();
			productList.add(new ProductResponseDTO(product));
		}
		
		for (Iterator iterator = gym.getCityList().iterator(); iterator.hasNext();) {
			City city = (City) iterator.next();
			cityList.add(new CityResponseDTO(city));
		}
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAdress() {
		return adress;
	}


	public void setAdress(String adress) {
		this.adress = adress;
	}


	public List<ProductResponseDTO> getProductList() {
		return productList;
	}


	public void setProductList(List<ProductResponseDTO> productList) {
		this.productList = productList;
	}


	public List<CityResponseDTO> getCityList() {
		return cityList;
	}


	public void setCityList(List<CityResponseDTO> cityList) {
		this.cityList = cityList;
	}
	
	
	
	
}
