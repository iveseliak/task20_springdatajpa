package com.example.first.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.first.entity.ProductWhithCount;

public class OrderRequestDTO {
	
	private List<ProductWhithCount> productWithCount=new ArrayList<>();

	private Long price;

	private Long userId;

	
	
	

	public List<ProductWhithCount> getProductWithCount() {
		return productWithCount;
	}

	public void setProductWithCount(List<ProductWhithCount> productWithCount) {
		this.productWithCount = productWithCount;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	

}
