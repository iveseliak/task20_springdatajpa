package com.example.first.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.first.dto.CategoryRequestDTO;
import com.example.first.dto.CategoryResponseDTO;
import com.example.first.entity.Category;
import com.example.first.service.CategoryService;

@RestController
@CrossOrigin
@RequestMapping("/category")
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;

	@PostMapping
	public CategoryResponseDTO save(@RequestBody @Valid CategoryRequestDTO categoryRequestDTO) {
		return categoryService.save(categoryRequestDTO);
	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id) {
		categoryService.delete(id);
	}

	@PutMapping("/{id}")
	CategoryResponseDTO update(@PathVariable Long id, @RequestBody @Valid CategoryRequestDTO categoryRequestDTO) {
		return categoryService.update(id, categoryRequestDTO);
	}

	@GetMapping
	List<CategoryResponseDTO> findAll() {
		return categoryService.findAll();
	}

}
