package com.example.first.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.first.dto.ConversationRequestDTO;
import com.example.first.dto.ConversationResponseDTO;
import com.example.first.dto.GymRequestDTO;
import com.example.first.dto.GymResponseDTO;
import com.example.first.service.GymService;

@RestController
@CrossOrigin
@RequestMapping("/gym")
public class GymController {
	
	@Autowired
	GymService gymService;
	
	@PostMapping
	public GymResponseDTO save(@RequestBody GymRequestDTO gymRequestDTO) {
		return gymService.save(gymRequestDTO);
	}

	@PutMapping("/{id}")
	public GymResponseDTO update(@PathVariable Long id, @RequestBody GymRequestDTO gymRequestDTO) {

		return gymService.update(id, gymRequestDTO);

	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id) {
		gymService.delete(id);
	}

	@GetMapping
	List<GymResponseDTO> findAll() {
		return gymService.findAll();
	}
	

}
