package com.example.first.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.first.dto.MessageRequestDTO;
import com.example.first.dto.MessageResponseDTO;
import com.example.first.service.MessageService;

@Controller
@RequestMapping("/message")
public class MessageController {
	@Autowired
	private MessageService messageService;

	@PostMapping
	public MessageResponseDTO createMessage(@RequestBody MessageRequestDTO messageRequest) {
		return messageService.save(messageRequest);
	}

	@DeleteMapping("/{id}")
	public void deleteMessage(@PathVariable("id") Long messageId) {
		messageService.delete(messageId);
	}



}
