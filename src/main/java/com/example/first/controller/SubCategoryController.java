package com.example.first.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.first.dto.SubCategoryRequestDTO;
import com.example.first.dto.SubCategoryResponseDTO;
import com.example.first.entity.SubCategory;
import com.example.first.service.SubCategoryService;

@RestController
	@CrossOrigin
	@RequestMapping("/subCategory")
	public class SubCategoryController{

		@Autowired
		private SubCategoryService subCategoryService;

		@PostMapping()
		public SubCategoryResponseDTO save(@RequestBody @Valid SubCategoryRequestDTO subCategoryRequestDTO) {
			return subCategoryService.save(subCategoryRequestDTO);
		}

		@PutMapping("/{id}")
		public SubCategoryResponseDTO update(@PathVariable Long id, @RequestBody @Valid SubCategoryRequestDTO subCategoryRequestDTO) {
			return subCategoryService.update(id, subCategoryRequestDTO);
		}

		@DeleteMapping("/{id}")
		void delete(@PathVariable Long id) {
			subCategoryService.delete(id);
		}

		@GetMapping
		List<SubCategoryResponseDTO> findAll() {
			return subCategoryService.findAll();
		}

	}


