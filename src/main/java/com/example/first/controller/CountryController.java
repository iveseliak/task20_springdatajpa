package com.example.first.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.first.dto.CountryRequestDTO;
import com.example.first.dto.CountryResponseDTO;
import com.example.first.entity.Country;
import com.example.first.service.CountryService;

@RestController
@CrossOrigin
@RequestMapping("/country")
public class CountryController {
	
	@Autowired
	private CountryService countryService;

	@PostMapping
	public CountryResponseDTO save(@RequestBody @Valid CountryRequestDTO categoryRequestDTO) {
		return countryService.save(categoryRequestDTO);
	}


	@PutMapping("/{id}")
	public CountryResponseDTO update(@PathVariable Long id, @RequestBody @Valid CountryRequestDTO categoryRequestDTO) {

		return countryService.update(id, categoryRequestDTO);
	}

	void delete(@PathVariable Long id) {
		countryService.delete(id);
	}

}
