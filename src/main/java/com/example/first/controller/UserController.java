package com.example.first.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.first.dto.UserRequestDTO;
import com.example.first.dto.UserResponseDTO;
import com.example.first.entity.User;
import com.example.first.service.UserService;

@RestController
@RequestMapping("/person")
@CrossOrigin
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping
	public UserResponseDTO save(@RequestBody UserRequestDTO userRequestDTO) {
		return userService.save(userRequestDTO);
	}

	@GetMapping
	public List<UserResponseDTO> findAll() {
		return userService.findAll();
	}

	@PutMapping("/{id}")
	public UserResponseDTO update(@PathVariable Long id, @RequestBody UserRequestDTO userRequestDTO) {
		return userService.update(id, userRequestDTO);
	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id) {
		userService.delete(id);
	}

}

