package com.example.first.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.first.dto.GymRequestDTO;
import com.example.first.dto.GymResponseDTO;
import com.example.first.dto.OrderRequestDTO;
import com.example.first.dto.OrderResponseDTO;
import com.example.first.service.OrderService;

@RestController
@CrossOrigin
@RequestMapping("/order")
public class OrderController {
	
	@Autowired
	OrderService orderService;
	
	@PostMapping
	public OrderResponseDTO save(@RequestBody OrderRequestDTO orderRequestDTO) {
		return orderService.save(orderRequestDTO);
	}

	@PutMapping("/{id}")
	public OrderResponseDTO update(@PathVariable Long id, @RequestBody OrderRequestDTO orderRequestDTO) {

		return orderService.update(id, orderRequestDTO);

	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id) {
		orderService.delete(id);
	}

	@GetMapping
	List<OrderResponseDTO> findAll() {
		return orderService.findAll();
	}

}
