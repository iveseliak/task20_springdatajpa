package com.example.first.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.first.dto.UserInfoRequestDTO;
import com.example.first.dto.UserInfoResponseDTO;
import com.example.first.service.UserInfoService;

@RestController
@CrossOrigin
@RequestMapping("/userInfo")
public class UserInfoController {
	
	
	@Autowired
	UserInfoService userInfoService;
	
	@PostMapping
	public UserInfoResponseDTO save(@RequestBody UserInfoRequestDTO userInfoRequestDTO) {
		return userInfoService.save(userInfoRequestDTO);
	}

	@PutMapping("/{id}")
	public UserInfoResponseDTO update(@PathVariable Long id, @RequestBody UserInfoRequestDTO userInfoRequestDTO) {

		return userInfoService.update(id, userInfoRequestDTO);

	}

	
	
	
}
