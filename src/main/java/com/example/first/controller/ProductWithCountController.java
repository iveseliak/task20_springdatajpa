package com.example.first.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.first.dto.ProductResponseDTO;
import com.example.first.dto.ProductWhithCountRequestDTO;
import com.example.first.dto.ProductWhithCountResponseDTO;
import com.example.first.service.ProductWhithCountService;

@RestController
@CrossOrigin
@RequestMapping("/productWhithCount")
public class ProductWithCountController {
	
	@Autowired
	ProductWhithCountService productWhithCountService;
	
	@PostMapping
	public ProductWhithCountResponseDTO save(@RequestBody ProductWhithCountRequestDTO productWhithCountRequestDTO) {
		return productWhithCountService.save(productWhithCountRequestDTO);
	}

	@PutMapping("/{id}")
	public ProductWhithCountResponseDTO update(@PathVariable Long id, @RequestBody ProductWhithCountRequestDTO productWhithCountRequestDTO) {

		return productWhithCountService.update(id, productWhithCountRequestDTO);

	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id) {
		productWhithCountService.delete(id);
	}

	@GetMapping
	List<ProductWhithCountResponseDTO> findAll() {
		return productWhithCountService.findAll();
	}

}
