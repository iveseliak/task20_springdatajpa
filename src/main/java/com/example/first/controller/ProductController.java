package com.example.first.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.first.dto.ProductRequestDTO;
import com.example.first.dto.ProductResponseDTO;
import com.example.first.service.ProductService;

@RestController
@CrossOrigin
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	@PostMapping
	public ProductResponseDTO save(@RequestBody ProductRequestDTO productRequestDTO) {
		return productService.save(productRequestDTO);
	}

	@PutMapping("/{id}")
	public ProductResponseDTO update(@PathVariable Long id, @RequestBody ProductRequestDTO productRequestDTO) {

		return productService.update(id, productRequestDTO);

	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id) {
		productService.delete(id);
	}

	@GetMapping
	List<ProductResponseDTO> findAll() {
		return productService.findAll();
	}
	

}
