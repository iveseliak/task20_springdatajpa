package com.example.first.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.first.dto.CityRequestDTO;
import com.example.first.dto.CityResponseDTO;
import com.example.first.dto.ConversationRequestDTO;
import com.example.first.dto.ConversationResponseDTO;
import com.example.first.service.ConversationService;

@RestController
@CrossOrigin
@RequestMapping("/conversation")
public class ConversationController {

	@Autowired
	private ConversationService conversationService;

	@PostMapping
	public ConversationResponseDTO save(@RequestBody ConversationRequestDTO conversationRequestDTO) {
		return conversationService.save(conversationRequestDTO);
	}

	@PutMapping("/{id}")
	public ConversationResponseDTO update(@PathVariable Long id, @RequestBody ConversationRequestDTO conversationRequestDTO) {

		return conversationService.update(id, conversationRequestDTO);

	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id) {
		conversationService.delete(id);
	}

	@GetMapping
	List<ConversationResponseDTO> findAll() {
		return conversationService.findAll();
	}

}