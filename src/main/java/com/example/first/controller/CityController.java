package com.example.first.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.first.dto.CityRequestDTO;
import com.example.first.dto.CityResponseDTO;
import com.example.first.entity.City;
import com.example.first.service.CityService;


@RestController
@CrossOrigin
@RequestMapping("/city")
public class CityController {

	@Autowired
	private CityService cityService;

	@PostMapping
	public CityResponseDTO save(@RequestBody CityRequestDTO cityRequestDTO) {
		return cityService.save(cityRequestDTO);
	}

	@PutMapping("/{id}")
	public CityResponseDTO update(@PathVariable Long id, @RequestBody CityRequestDTO cityRequestDTO) {

		return cityService.update(id, cityRequestDTO);

	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id) {
		cityService.delete(id);
	}

	@GetMapping
	List<CityResponseDTO> findAll() {
		return cityService.findAll();
	}

}
